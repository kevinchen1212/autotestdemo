package com.test.pages;

import com.test.common.BaseInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Baidu extends BaseInit {

    @FindBy(id = "kw")
    public WebElement inputBox;

    @FindBy(id = "su")
    public WebElement searchButton;

    public void search(String value) throws Exception{
        inputBox.sendKeys(value);

        searchButton.click();

        Thread.sleep(6000);
    }

}
