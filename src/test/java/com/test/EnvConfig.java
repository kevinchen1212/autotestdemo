package com.test;


public class EnvConfig {

    //配置脚本执行环境
    public static String environment = "http://baidu.com";

    //获取附件所在目录
    public static String getFilePath() {

        //测试附件所在目录
        String filePath=null;

        //如果是windows，filePath取项目所在目录下的\src\test\resources\testFile\
        //如果是Linux，由于Hooks中指定使用RemoteWebDriver方式，对应附件在远程机下的/home/seluser/目录
        if(System.getProperty("os.name").contains("Windows"))
            filePath=System.getProperty("user.dir")+"\\src\\test\\resources\\testFile\\";
        else if(System.getProperty("os.name").contains("Linux"))
            filePath="/home/seluser/";

        return filePath;
    }


}
