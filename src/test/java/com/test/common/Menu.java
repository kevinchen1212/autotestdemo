package com.test.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Menu extends BaseInit {


    //顶部导航链接：组件
    @FindBy(xpath = "//span[text()='Pentest Activity']/../..")
    public WebElement pentestMenu;

    @FindBy(xpath = "//span[text()='Launch Scans']/..")
    public WebElement launchScansMenu;

    @FindBy(xpath = "//span[text()=' Dynamic Scans ']/../..")
    public WebElement launchDynamicScans;



    @FindBy(xpath = "//span[text()='Dynamic Scans']/../..")
    public WebElement dynamicScans;

    @FindBy(xpath = "//span[text()=' ZAP Scans ']/../..")
    public WebElement ZAPScans;

    // 顶部导航链接，传入参数：导航名称
    public void leftMenu(String menu) {

        wait.until(ExpectedConditions.visibilityOf(pentestMenu));

        switch (menu)
        {
            case "Pentest Activity":pentestMenu.click();break;
            case "Launch Scans":launchScansMenu.click();break;
            case "launchDynamicScans":launchDynamicScans.click();break;
            case "DynamicScans":dynamicScans.click();break;
            case "ZAPScans":ZAPScans.click();break;
        }

    }

}
