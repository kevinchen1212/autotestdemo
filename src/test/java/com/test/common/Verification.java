package com.test.common;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Verification extends BaseInit {

    public static String recName;

    // 验证当前page是否包含了特定内容
    public void verifyCurrentPage(String value) throws InterruptedException {
        Thread.sleep(2000);
        Assert.assertTrue(driver.getPageSource().contains(value));
    }

    // 验证当前alert是否包含了某个值
    public void verfiyCurrentAlert(String alertMessage){
        wait.until(ExpectedConditions.alertIsPresent());
        Assert.assertTrue(driver.switchTo().alert().getText().contains(alertMessage));
    }

    // 验证系统提示信息，参数ExpectedTips为期望的提示
    public void verifyMessage(String ExpectedTips){

        List<WebElement> actualTips= wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@id='cdk-overlay-0' or @id='cdk-overlay-1' or @id='cdk-overlay-2']//span")));

        if(actualTips.size()>1 && isElementExsit(driver, By.xpath("//a[contains(text(), '个人中心')]")))
            Assert.assertEquals(ExpectedTips, actualTips.get(1).getText());  //登录后的页面，顶部提示使用get(1)
        else
            Assert.assertEquals(ExpectedTips, actualTips.get(0).getText());  //未登录的页面，顶部提示使用get(0)

    }

    // 验证按钮是否处于不可点击状态，WebElement element为按钮元素
    public void verifyButtonStatus(WebElement element) {
        Assert.assertFalse(element.isEnabled());
    }



    //逐一对比是否倒序
    public void timeCompare( List<WebElement> list) throws Throwable {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time1,time2;
        System.out.println("数量："+list.size());
        System.out.println("第一个元素："+list.get(0).getText());
        for(int i=0;i<list.size()-1;i++){
            // 两个参数，先解析成Date格式
            time1=format.parse(list.get(i).getText());
            time2=format.parse(list.get(i+1).getText());
            System.out.println("第"+i+"次，时间："+list.get(i).getText()+" 与 时间"+list.get(i+1).getText()+"比较");

            Assert.assertTrue( time1.getTime() >= time2.getTime());

        }

    }

    public boolean isElementExsit(WebDriver driver,By locator)
    {
        try {
            driver.findElement(locator);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    // 对selenium-java里的ExpectedConditions下的invisibilityOf()进行重写
    // 当元素不存在时，也属于不可见
    public static ExpectedCondition<Boolean> invisibilityOfElement(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    return !element.isDisplayed();
                } catch (NoSuchElementException var1) {
                    System.out.println("元素不存在");
                    return true;
                } catch (StaleElementReferenceException var2) {
                    return true;
                }
            }

            public String toString() {
                return "invisibility of " + element;
            }
        };
    }

    public static ExpectedCondition<WebElement> elementToBeClickable(final WebElement element) {
        return new ExpectedCondition<WebElement>() {

            @Override
            public WebElement apply(WebDriver driver) {
                WebElement visibleElement = ExpectedConditions.visibilityOf(element).apply(driver);
                try {
                    if (visibleElement != null && visibleElement.isEnabled()) {
                        System.out.println("visibleElement.isEnabled():"+visibleElement.isEnabled());
                        return visibleElement;
                    }
                    return null;
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "element to be clickable: " + element;
            }
        };
    }

}
