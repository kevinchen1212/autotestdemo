package com.test.stepDefs;

import com.test.Hooks;
import com.test.pages.Baidu;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class baiduStep {

    public Baidu baidu;

    public baiduStep(Baidu baidu) {
        this.baidu= baidu;

    }
    @Given("^打开百度页面$")
    public void 打开百度页面() throws Throwable {
        Hooks.driver.get("http://www.baidu.com");

    }

    @When("^在输入框中，输入\"([^\"]*)\",然后点击【百度一下】按钮$")
    public void 在输入框中输入然后点击百度一下按钮(String value) throws Throwable {
        baidu.search(value);
    }
}
